import CartParser from './CartParser';
import { readFileSync } from 'fs';

let parser, calcTotal;

beforeEach(() => {
	parser = new CartParser();
	calcTotal = parser.calcTotal;
});

describe('CartParser - unit tests', () => {
	it('should throw error when there is at least one validation error', () => {
		parser.readFile = jest.fn();
		parser.validate = jest.fn(() => [{}]);

		expect(() => parser.parse()).toThrowError('Validation failed!');
	});
});

describe('validation', () => {
	it('should return an empty array when there is no errors', () => {
		const csv = `Product name,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		expect(parser.validate(csv)).toEqual([]);
	});

	it('should return an array with error object type: "header"', () => {
		const csv = `Product,Price,Quantity
		Mollis consequat,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const errors = parser.validate(csv);
		expect(errors[0]).toEqual({
			type: 'header',
			row: 0,
			column: 0,
			message: 'Expected header to be named "Product name" but received Product.'
		});
	});

	it('should return an array with with error object type: "row" when row length is shorter than expected', () => {
		const csv = `Product name,Price,Quantity
		Mollis consequat,9.00
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const errors = parser.validate(csv);
		expect(errors[0]).toEqual({
			type: 'row',
			row: 1,
			column: -1,
			message: 'Expected row to have 3 cells but received 2.'
		});
	});

	it('should return an array with error object with prop type: cell when cell is empty string', () => {
		const csv = `Product name,Price,Quantity
		,9.00,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const errors = parser.validate(csv);
		expect(errors[0]).toEqual({
			type: 'cell',
			row: 1,
			column: 0,
			message: 'Expected cell to be a nonempty string but received "".'
		});
	});

	it('should return an array with error object with prop type: "cell" when cell is not a number', () => {
		const csv = `Product name,Price,Quantity
		Mollis consequat,abc,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const errors = parser.validate(csv);
		expect(errors[0]).toEqual({
			type: 'cell',
			row: 1,
			column: 1,
			message: 'Expected cell to be a positive number but received "abc".'
		});
	});

	it('should return an array with error object with prop type: "cell" when cell is negative number', () => {
		const csv = `Product name,Price,Quantity
		Mollis consequat,-5,2
		Tvoluptatem,10.32,1
		Scelerisque lacinia,18.90,1
		Consectetur adipiscing,28.72,10
		Condimentum aliquet,13.90,1`;

		const errors = parser.validate(csv);
		expect(errors[0]).toEqual({
			type: 'cell',
			row: 1,
			column: 1,
			message: 'Expected cell to be a positive number but received "-5".'
		});
	});
});

describe('line parsing', () => {
	it('should retun an object when string is passed', () => {
		const line = 'Mollis consequat,9.00,2';

		expect(parser.parseLine(line)).toEqual(expect.objectContaining(
			{
				"id": expect.anything(),
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			}
		));
	});
});

describe('calculate total', () => {
	it('should be equal to given number', () => {
		const items = [
			{
				price: 10,
				quantity: 2
			},
			{
				price: 3,
				quantity: 7
			}
		];

		expect(calcTotal(items)).toBe(41);
	});
});

describe('CartParser - integration test', () => {
	it('should read .csv-file and return an object that is eqaul to given', () => {
		const path = '__dirname/../samples/cart.csv';
		const result = {
			"items": [
					{
							"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
							"name": "Mollis consequat",
							"price": 9,
							"quantity": 2
					},
					{
							"id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
							"name": "Tvoluptatem",
							"price": 10.32,
							"quantity": 1
					},
					{
							"id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
							"name": "Scelerisque lacinia",
							"price": 18.9,
							"quantity": 1
					},
					{
							"id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
							"name": "Consectetur adipiscing",
							"price": 28.72,
							"quantity": 10
					},
					{
							"id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
							"name": "Condimentum aliquet",
							"price": 13.9,
							"quantity": 1
					}
			],
			"total": 348.32
		};

		const id = expect.anything();
		result.items.forEach(item => item.id = id);

		expect(parser.parse(path)).toEqual(result);
	})
});